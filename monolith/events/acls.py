import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {"per_page": 1, "query": f"{city} {state}"}
    url = "https://api.pexels.com/v1/search"

    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)

    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_weather_data(city, state):
    # Create the URL for the geocoding API with the city and state
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state}&appid={OPEN_WEATHER_API_KEY}"
    # Make the request
    response = requests.get(url)
    # Parse the JSON response
    content = json.loads(response.content)
    # Get the latitude and longitude from the response
    lat = None
    lon = None
    try:
        lat = content[0]["lat"]
        lon = content[0]["lon"]
        print(lat, lon)
    except (KeyError, IndexError):
        print("Error")

    # Create the URL for the current weather API with the latitude
    #   and longitude
    url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
    # Make the request
    response = requests.get(url)
    # Parse the JSON response
    content = json.loads(response.content)
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    # Return the dictionary
    try:
        temp = content["main"]["temp"]
        desc = content["weather"][0]["description"]
        return {"temp": temp, "desc": desc}
    except (KeyError, IndexError):
        print("error")
